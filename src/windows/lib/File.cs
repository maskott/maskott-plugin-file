﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;

namespace MaskottFileHelperWinRT
{
    public sealed class File
    {
        private static async Task CopyFoldersAndFiles(StorageFolder src, StorageFolder dst)
        {
            System.Collections.Generic.IReadOnlyList<StorageFolder> folders = await src.GetFoldersAsync();
            foreach (StorageFolder folder in folders)
            {
                StorageFolder new_dest = await dst.CreateFolderAsync(folder.DisplayName);
                await CopyFoldersAndFiles(folder, new_dest);
            }
            System.Collections.Generic.IReadOnlyList<StorageFile> files = await src.GetFilesAsync();
            foreach (StorageFile file in files)
            {
                StorageFile new_file = await file.CopyAsync(dst);
            }
        }
        public static IAsyncAction MoveFolderAsync(StorageFolder src, StorageFolder dst, String name)
        {
            return MoveFolder(src, dst, name).AsAsyncAction();
        }

        private static async Task MoveFolder(StorageFolder src, StorageFolder dst, String name)
        {
            StorageFolder dest = await dst.CreateFolderAsync(name);
            await CopyFoldersAndFiles(src, dest);
            await src.DeleteAsync();
        }
    }
}
